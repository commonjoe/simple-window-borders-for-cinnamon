# Simple Window Borders for Cinnamon

These are only simple window borders for Cinnamon written. I've tested them in Linux Mint 18.3 and 19.2.

![Sample Sample](Sample.png)

## Features

All Features are Variables at the Top of the XML File.

You can easily change:
* Height and Width of Icons
* Colors of Icons
* Color of Title Bar (Both Focused and Unfocused)
* Color of Title (Both Focused and Unfocused)
* How Rounded the Title Bar Is
* Other Subtleties

Other Features of Note:

* There are special enhancements so you know which window has focus
  * Color changes
  * A extra line is drawn underneath the focused window title bar
* All Six Major Icons Include: Close, Maximize, Minimize, Menu, Shade, Stick
  * Mousing over the buttons will cause them to be highlighted
  * If you "stick" the window, the icon changes so you know it is stuck
  * If you roll up the shade, the icon changes
* There are no svg or graphic files used
* Released under Public Domain. It's so simple, I don't care what you do with it.

Version 1.0.1 Changes:

* Updated the color red so the focused window is easier to see
* Updated the previews to reflect the new red color
* Vertically centered the title better in all six colors (Thank you Smurphos!)
* The original six colors of version 1 were created by hand. The six colors were created by template in Version 1.0.1.